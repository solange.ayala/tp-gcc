from gcc.settings import *

DATABASES = {
    'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': 'adoptapp',
       'USER': 'postgres',
       'PASSWORD': 'postgres',
       'HOST': 'postgres',
       'PORT': '5432',
   },
}