from django.test import TestCase
from django.contrib.auth.models import User


class Test(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            username='Solange',
            email='solange.ayala@gmail.com',
            password='asd'
        )

    def test_uni(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


    def test_login(self):
        response = self.client.post("/login/", data={
            "name": "Solange",
            "password": "asd"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/")

