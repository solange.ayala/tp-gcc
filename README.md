**ADOPTAPP**

*La aplicación consiste en un sistema de adopción de mascotas, existe una página principal donde se podrá observar toda la información sobre adopciones, etc.
Para ingresar al sistema, el usuario deberá autenticarse via username y password, en caso de que las credenciales ingresadas no sean válidas, no podrá visualizar la página principal.*



**Documentación sobre el despliegue de la aplicación**

Tareas elaboradas por la herramienta de integración continua: 


*  CHEQUEO: chequea que no existan errores en dependencias o estructura de la aplicación.


*  DESARROLLO: la rama developer se deploya en un servidor distinto al de producción, en este caso se despliega en [https://adoptapp.herokuapp.com](https://adoptapp.herokuapp.com).


*  HOMOLOGACIÓN: se ejecutan las pruebas unitarias propias desarrolladas por el framework que se utilizó para desarrollar la aplicación.


*  PRODUCCIÓN: se deploya la aplicación en su versión producción, de la rama master, se deploya en [http://adoptapp-prod.herokuapp.com](http://adoptapp-prod.herokuapp.com)

